from datetime import datetime
from django.contrib import admin
from django.db.models import Q
from .models import *

def iniciar_tarea(modeladmin, request, queryset):
    for tarea in queryset:
        bitacora = Bitacora()
        bitacora.tarea = tarea
        bitacora.save()

def finalizar_tarea(modeladmin, request, queryset):
    queryset.update(terminada=True)

def finalizar_bitacora(modeladmin, request, queryset):
    queryset.update(fin=datetime.now())

class TareaInLine(admin.TabularInline):
    model = Tarea
    extra = 1


class BitacoraInLine(admin.StackedInline):
    model = Bitacora
    extra = 1


@admin.register(Proyecto)
class ProyectoAdmin(admin.ModelAdmin):
    inlines = [TareaInLine]


@admin.register(Tarea)
class TareaAdmin(admin.ModelAdmin):
    list_display = [
        'nombre',
        'descripcion',
        'proyecto',
        'terminada',
        'fecha_de_entrega',
        'usuario',
        'responsable'
    ]
    actions = [iniciar_tarea, finalizar_tarea]
    list_filter = ['responsable', 'terminada']
    inlines = [BitacoraInLine]

    def get_queryset(self, request):
        qs = super(TareaAdmin, self).get_queryset(request)
        if not request.user.is_superuser:
            qs = qs.filter(responsable=request.user)
        return qs

    def save_model(self, request, obj, form, change):
        if not request.user.is_superuser:
            obj.responsable = request.user
        obj.save()


@admin.register(Bitacora)
class BitacoraAdmin(admin.ModelAdmin):
    list_display = [
        'tarea',
        'inicio',
        'fin'
    ]
    actions = [finalizar_bitacora]

    def get_queryset(self, request):
        qs = super(BitacoraAdmin, self).get_queryset(request)
        if not request.user.is_superuser:
            qs = qs.filter(Q(tarea__responsable__id=request.user.id) | Q(tarea__usuario__id=request.user.id))
        return qs
