from django.db import models
from django.contrib.auth.models import User

class Proyecto(models.Model):
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return self.nombre


class Tarea(models.Model):
    nombre = models.CharField(max_length=50)
    descripcion = models.TextField(null=True, blank=True)
    proyecto = models.ForeignKey(Proyecto, related_name='tareas')
    terminada = models.BooleanField(default=False)
    fecha_de_entrega = models.DateField(null=True, blank=True)
    usuario = models.ForeignKey(User, related_name='tareas_solicitadas', null=True, blank=True)
    responsable = models.ForeignKey(User, related_name='tareas_pendientes', null=True, blank=True)

    def __str__(self):
        return self.nombre


class Bitacora(models.Model):
    tarea = models.ForeignKey(Tarea, related_name='actividad')
    inicio = models.DateTimeField(auto_now_add=True)
    fin = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return str(self.tarea)
