from django.core.management.base import BaseCommand, CommandError
from apps.bitacora.models import *

class Command(BaseCommand):
    help = 'Lee un modelo'

    def add_arguments(self, parser):
        parser.add_argument('modelo', nargs='+', type=str)

    def handle(self, *args, **options):
        #importar = ['apps.bitacora.models.%s' % x for x in options['modelo']]
        #modelos = map(__import__, importar)

        for modelo in options['modelo']:
            self.stdout.write(modelo)
