from rest_framework import serializers
from .models import *

class ProyectoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Proyecto


class TareaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tarea


class BitacoraSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bitacora
