# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('bitacora', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='tarea',
            name='responsable',
            field=models.ForeignKey(related_name='tareas_pendientes', to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='tarea',
            name='usuario',
            field=models.ForeignKey(related_name='tareas_solicitadas', to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='bitacora',
            name='tarea',
            field=models.ForeignKey(related_name='actividad', to='bitacora.Tarea'),
        ),
        migrations.AlterField(
            model_name='tarea',
            name='proyecto',
            field=models.ForeignKey(related_name='tareas', to='bitacora.Proyecto'),
        ),
    ]
