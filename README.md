# pepepecas

Prototipo para la administración de tareas.

## Caracteristicas

Se trabaja en el sitio de administración de django con los siguientes modelos:

- Proyectos
- Tareas
- Bitacora de actividad

## To do

- Fecha y hora de creación de la tarea
- Permitir adjuntar un archivo a cada tarea
- Comentarios para las tareas, cada comentario puede adjuntar un archivo